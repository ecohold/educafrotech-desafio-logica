# educafrotech-desafio-logica

## Desafio Final - Lógica de Programação - Energia Solar

**Personagens:**

* Simba
* Nala
* Kiara
* Kion
* Timão
* Pumba

**Contexto:**

Simba e Nala, que nasceram com o conhecimento do efeito fotoelétrico, explicam essa fascinante descoberta aos seus filhos Kiara e Kion, de dez anos. Depois, a família vai ao restaurante "Floresta" de Timão e Pumba para um delicioso almoço.
Timão garçon, oferece o prato de sempre, Simba aceita e grita..."O de semmmpre"
Pumba chega correndo, serve a familia, 
Em seguida, Timão passa o cartão black plimm
Conta atualizada, saldo e crédito atualizado.

**Atividades:**
Algoritmo em Portugoal
Programa em egautech ou delegua, filha de eguatech

**1. Programa para Saldo na Conta:**

**Descrição:**

Este programa simula o saque de dinheiro da conta de Leãozinho, utilizando seu cartão Black Plimm. O programa verifica o saldo disponível e informa se o saque é possível, além de mostrar o saldo restante após a transação.

**Entradas:**

* Valor a ser sacado

**Saídas:**

* Mensagem "Saldo disponível" ou "Saldo insuficiente"
* Saldo restante na conta

**Algoritmo Portugol:**

```portugol
programa saldo_conta

// Declaração de variáveis
real saldo_inicial, valor_saque, saldo_final

// Valor inicial da conta
saldo_inicial = 100000.00

// Leitura do valor a ser sacado
escreva("Digite o valor a ser sacado: R$ ")
leia(valor_saque)

// Verificação do saldo
se valor_saque <= saldo_inicial então
  // Saldo suficiente
  saldo_final = saldo_inicial - valor_saque
  escreva("Saldo disponível")
  escreva("Saldo restante: R$ ", saldo_final)
senao
  // Saldo insuficiente
  escreva("Saldo insuficiente")
  escreva("Saldo atual: R$ ", saldo_inicial)
fim_se

fim_programa
```

**2. Menu do Restaurante Floresta:**

**Descrição:**

Este programa simula o menu do restaurante "Floresta", de propriedade de Timão e Pumba. O programa exibe as opções de pratos disponíveis e seus respectivos preços, permitindo que Leãozinho e Nala escolham um prato e calculem o valor total da conta para duas pessoas.

**Entradas:**

* Opção do prato (1, 2 ou 3)

**Saídas:**

* Descrição do prato escolhido
* Valor total da conta para duas pessoas

**Algoritmo Portugol:**

```portugol
programa menu_restaurante

// Declaração de variáveis
inteiro opcao_prato
real valor_total

// Opções do menu
escreva("**Menu Restaurante Floresta**")
escreva("1 - Lagosta ao Molho Rosé (R$ 120,00 por pessoa)")
escreva("2 - Moqueca de Camarão (R$ 100,00 por pessoa)")
escreva("3 - Filé de Peixe com Arroz Branco (R$ 50,00 por pessoa)")

// Leitura da opção do prato
escreva("Digite a opção do prato: ")
leia(opcao_prato)

// Cálculo do valor total
se opcao_prato = 1 então
  valor_total = 240.00
senao se opcao_prato = 2 então
  valor_total = 200.00
senao se opcao_prato = 3 então
  valor_total = 100.00
senao
  // Opção inválida
  escreva("Opção inválida!")
fim_se

// Exibição do resultado
se opcao_prato > 0 e opcao_prato < 4 então
  escreva("Prato escolhido: ", opcao_prato)
  escreva("Valor total da conta para duas pessoas: R$ ", valor_total)
fim_se

fim_programa
```
